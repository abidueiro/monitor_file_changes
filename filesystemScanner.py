import os
import hashlib

from pathlib import Path
from mydatabasemanagerfile import MyDatabaseManager


path = "test"
isFolder = True
fullList = []
currhash = ""
db_name = "parenttest.db"
table_name = "ArchLinux"

my_current_db = MyDatabaseManager(db_name, table_name)

#### Calculate hash

def md5_update_from_dir(directory, my_hash):
    #assert Path(directory).is_dir()
    for hash_path in sorted(Path(directory).iterdir(), key=lambda p: str(p).lower()):
        my_hash.update(hash_path.name.encode())
        if hash_path.is_file():
            with open(hash_path, "rb") as f:
                for chunk in iter(lambda: f.read(4096), b""):
                    my_hash.update(chunk)
        elif hash_path.is_dir():
            my_hash = md5_update_from_dir(hash_path, my_hash)
    return my_hash


def md5_dir(directory):
    return md5_update_from_dir(directory, hashlib.md5()).hexdigest()


def file_hasher(hashing_file):
    try:
        if not isFolder:
            with open(hashing_file, "rb") as current:
                readfile = current.read()
                current_hash = hashlib.md5(readfile).hexdigest()
                return current_hash
    except PermissionError:
        current_hash = "Could not hash due to permission"
        return current_hash

#####test
#print(os.getcwd())
#####

for (dirpath, dirnames, filenames) in os.walk(path):
    isFolder = True
    # full_dirpath = dirpath + "\\"
    full_dirpath = dirpath
    file_stats = os.stat(dirpath)
    size = round(file_stats.st_size / (1024 * 1024), 2)
    fullList.append([full_dirpath, size, md5_dir(full_dirpath), isFolder])
    for file in filenames:
        # file_path = dirpath + "\\" + file
        file_path = dirpath + "/" + file
        isFolder = False
        file_stats = os.stat(file_path)
        size = round(file_stats.st_size / (1024 * 1024), 2)
        fullList.append([file_path, size, file_hasher(file_path), isFolder])
        #print(size)

my_current_db.initialize_db()
for file_info in fullList:
    my_current_db.my_updater(file_info[0], file_info[1], file_info[2], file_info[3])

#TEST compare DATA

#result = my_current_db.compare()

    
#my_current_db.inserting_parentID()
#my_current_db.inserting_suffix()
my_current_db.close_db()